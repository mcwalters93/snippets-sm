import { Buffer } from "buffer";
import SyntaxHighlighter from "react-syntax-highlighter";
import { atomOneDark } from "react-syntax-highlighter/dist/esm/styles/hljs";
import IconTextButton from "./IconTextButton";
import LanguageBadge from "./LanguageBadge";
import styles from "./SnippetCard.module.css";

// Map language keys to their SyntaxHighlighter styles
// See https://github.com/react-syntax-highlighter/react-syntax-highlighter
const LanguageStyleMap = {
  js: "javascript",
  go: "go",
  py: "python",
};

// Try and decode the snippet from base64
function tryDecodeSnippet(raw) {
  try {
    return { decoded: Buffer.from(raw, "base64").toString(), err: null };
  } catch (err) {
    return { err: `Failed to parse Base64 encoded snippet: ${err.message}` };
  }
}

function SnippetCard(props) {
  const { language, author, title, body, description, votes } = props;

  // Default to plaintext if no supported language is supplied
  const syntax = LanguageStyleMap[language.key] || "plaintext";

  const { decoded, err } = tryDecodeSnippet(body);

  if (err) {
    // For now, don't render any broken snippets
    console.error(err);
    return null;
  }

  return (
    <div className={styles.Card}>
      <LanguageBadge language={language} />
      <header>
        <h2 className="mdc-typography--headline4">{title}</h2>
        <IconTextButton icon="arrow-up" label="Upvote" onClick={() => {}} />
      </header>
      <p className="mdc-typography--subtitle2">By {author.name}</p>
      <p className="mdc-typography--body2">Votes: {votes}</p>
      <p className="mdc-typography--body2">{description}</p>
      <SyntaxHighlighter language={syntax} style={atomOneDark}>
        {decoded}
      </SyntaxHighlighter>
    </div>
  );
}

export default SnippetCard;
