import { useEffect, useState } from "react";
import { useFetch } from "../api/useFetch.hook";
import styles from "./Snippets.module.css";
import SnippetCardContainer from "./SnippetCard.container";

function SnippetsContainer() {
  const [makeRequest, { data, error }] = useFetch();

  // See https://github.com/typicode/json-server#relationships
  // This expands the author and language objects so we
  // don't have to fetch them by id subsequently
  const [endpoint] = useState(
    "/snippets?_expand=author&_expand=language&_sort=title&_order=desc",
  );

  // Initial fetch
  useEffect(() => {
    makeRequest(endpoint);
  }, [makeRequest, endpoint]);

  if (error) {
    // TODO: Display error nicely
    return <div>Error!</div>;
  }

  if (data) {
    return (
      <section className={styles.Container}>
        {data.map(snippet => (
          <SnippetCardContainer snippetId={snippet.id} key={snippet.id} />
        ))}
      </section>
    );
  }

  return null;
}

export default SnippetsContainer;
